package com.whitekit;

import com.whitekit.util.Broker;
import com.whitekit.util.SaveCache;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Runner {
    public static void main(String[] args){
        SaveCache saveCache = new SaveCache();
        Broker broker = new Broker();
        ExecutorService service = Executors.newFixedThreadPool(
                Runtime.getRuntime().availableProcessors());
        service.execute(saveCache);
        service.execute(broker);
        service.shutdown();
    }
}
