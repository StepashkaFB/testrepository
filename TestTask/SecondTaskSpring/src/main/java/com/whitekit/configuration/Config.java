package com.whitekit.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import java.util.Collections;
import java.util.List;

import static com.whitekit.constants.MainPropertyConstant.LOCALHOST;
import static com.whitekit.constants.MainPropertyConstant.NAME_BUCKET;
import static com.whitekit.constants.MainPropertyConstant.PASSWORD_BUCKET;
import static com.whitekit.constants.MainPropertyConstant.USERNAME_FOR_COUCHBASE;

@Configuration
@ComponentScan("com.whitekit")
@EnableCouchbaseRepositories("com.whitekit")
public class Config extends AbstractCouchbaseConfiguration {

    @Override
    protected List<String> getBootstrapHosts() {
        return Collections.singletonList(LOCALHOST);
    }

    @Override
    protected String getBucketName() {
        return NAME_BUCKET;
    }

    @Override
    protected String getBucketPassword() {
        return PASSWORD_BUCKET;
    }

    protected String getUsername() {
        return USERNAME_FOR_COUCHBASE;
    }
}
