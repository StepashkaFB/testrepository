package com.whitekit.service;

import com.whitekit.model.Rate;
import com.whitekit.repository.RateRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CashService {
    private final RateRepository rateRepository;

    public CashService(RateRepository rateRepository) {
        this.rateRepository = rateRepository;
    }

    public void save(Rate rate) {
        rateRepository.save(rate);
    }
    public List<Rate> getRatesBalance(Integer integer){
        return  rateRepository.getByBalanceLessThan(integer);
    }
}
