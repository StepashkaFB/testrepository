package com.whitekit.repository;

import com.whitekit.model.Rate;
import org.springframework.data.couchbase.repository.CouchbaseRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RateRepository extends CouchbaseRepository<Rate,String> {
    List<Rate> getByBalanceLessThan(Integer i);
}
