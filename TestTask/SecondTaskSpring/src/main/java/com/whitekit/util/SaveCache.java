package com.whitekit.util;

import com.whitekit.Runner;
import com.whitekit.configuration.Config;
import com.whitekit.model.Rate;
import com.whitekit.service.CashService;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.time.LocalTime;
import java.util.Collections;

import static com.whitekit.constants.MainPropertyConstant.HTTP_LOCALHOST_URL;
import static com.whitekit.constants.MainPropertyConstant.OUTPUT_XML;
import static com.whitekit.constants.MainPropertyConstant.POLLING_TIME;
import static com.whitekit.constants.MainPropertyConstant.SCHEMA_XSL;
@Log4j
public class SaveCache implements Runnable {

    private static final String  XML_ELEMENT = "field";

    @Override
    public void run() {
        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            try {
                convertingXML(getXMLResponse());
                ClassLoader classLoader = Runner.class.getClassLoader();
                try (StaxStreamProcessor processor =
                             new StaxStreamProcessor(new StreamSource(String.valueOf(
                                     classLoader.getResource(OUTPUT_XML))))) {
                    XMLStreamReader reader = processor.getReader();
                    while (reader.hasNext()) {
                        int event = reader.next();
                        if (event == XMLEvent.START_ELEMENT &&
                            XML_ELEMENT.equals(reader.getLocalName())) {
                            String string = reader.getElementText();
                            System.out.println(string);
                            saveRate(string);
                        }
                    }
                }
                Thread.sleep(POLLING_TIME);
            } catch (TransformerException | InterruptedException | XMLStreamException e) {
                log.error(e.getMessage());
            }
        }
    }
    private void saveRate(String str){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(Config.class);
        ctx.refresh();
        CashService cashService = ctx.getBean(CashService.class);
        cashService.save(new Rate(str));
        ctx.close();
    }
    private String getXMLResponse() {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_XML));
        HttpEntity<String> entity = new HttpEntity<>("parameters", headers);
        ResponseEntity<String> result = restTemplate.exchange(HTTP_LOCALHOST_URL,
                HttpMethod.GET, entity, String.class);
        return result.getBody();
    }

    private void convertingXML(String str) throws TransformerException {
        ClassLoader classLoader = Runner.class.getClassLoader();
        TransformerFactory factory = TransformerFactory.newInstance();
        Source xslt = new StreamSource(String.valueOf(classLoader.getResource(SCHEMA_XSL)));
        Transformer transformer = factory.newTransformer(xslt);
        Source xml = new StreamSource(new StringReader(str));
        transformer.transform(xml, new StreamResult(classLoader.getResource("") + OUTPUT_XML));
    }
}
