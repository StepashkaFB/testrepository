package com.whitekit.util;

import java.time.LocalTime;

class TimeWork {
    private static final LocalTime localTimeNow = LocalTime.now();
    static final LocalTime workTime = localTimeNow.plusSeconds(40);
}
