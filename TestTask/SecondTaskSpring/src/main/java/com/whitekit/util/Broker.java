package com.whitekit.util;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.whitekit.configuration.Config;
import com.whitekit.service.CashService;
import lombok.extern.log4j.Log4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;
import java.time.LocalTime;
import java.util.concurrent.TimeoutException;

import static com.whitekit.constants.MainPropertyConstant.MILLIS_BROKER;

@Log4j
public class Broker implements Runnable {
    private static final int BALANCE = 140;
    private final static String QUEUE_NAME = "users_balance_queue";
    private static final String LOCALHOST_BROKER = "localhost";

    @Override
    public void run() {
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(Config.class);
        ctx.refresh();
        CashService cashService = ctx.getBean(CashService.class);
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(LOCALHOST_BROKER);
        while (LocalTime.now().isBefore(TimeWork.workTime)) {
            String message = cashService.getRatesBalance(BALANCE).toString();
            try {
                Connection connection = factory.newConnection();
                Channel channel = connection.createChannel();
                channel.queueDeclare(QUEUE_NAME, false, false, false, null);
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
                System.out.println(" [x] Sent '" + message + "'");
                channel.close();
                connection.close();
                Thread.sleep(MILLIS_BROKER);
            } catch (IOException | TimeoutException | InterruptedException e) {
                log.error(e.getMessage());
            }
        }
        ctx.close();
    }
}
