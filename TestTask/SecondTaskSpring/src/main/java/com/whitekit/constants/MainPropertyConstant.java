package com.whitekit.constants;

public class MainPropertyConstant {
    //N milliseconds to poll the 1 application
    public static final int POLLING_TIME = 5000;
    //N milliseconds for broker
    public static final int MILLIS_BROKER = 4000;
    //URL for getting XML data from the 1 application
    public static final String HTTP_LOCALHOST_URL = "http://localhost:8081";
    // result after xslt conversion
    public static final String OUTPUT_XML = "output.xml";
    // XSLT schema
    public static final String SCHEMA_XSL = "article1a.xsl";

    public static final String LOCALHOST = "localhost";
    public static final String NAME_BUCKET = "TestBucket";
    //admin password
    public static final String PASSWORD_BUCKET = "123456";
    public static final String USERNAME_FOR_COUCHBASE = "Administrator";
}
