package com.whitekit.model;

import com.couchbase.client.java.repository.annotation.Field;
import com.couchbase.client.java.repository.annotation.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Rate {
    @Id
    private String id;
    @Field
    private String clientId;
    @Field
    private Integer balance;

    public Rate(String str) {
        String[] temp = str.split("/");
        id = temp[0];
        clientId = temp[0];
        balance = Integer.valueOf(temp[1]);
    }
}
