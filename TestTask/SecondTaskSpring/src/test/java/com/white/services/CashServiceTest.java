package com.white.services;

import com.white.config.Config;
import com.whitekit.model.Rate;
import com.whitekit.repository.RateRepository;
import com.whitekit.service.CashService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = Config.class,
        loader = AnnotationConfigContextLoader.class)
class CashServiceTest {
    @Autowired
    RateRepository rateRepository;
    @Autowired
    CashService cashService;
    private static List<Rate> rates = new ArrayList<>();
    private static Rate rate = mock(Rate.class);

    @BeforeAll
    static void setup() {
        rates.add(new Rate("1/100"));
        rates.add(new Rate("2/100"));
    }

    @Test
    void save() {
        when(rateRepository.save(any(Rate.class))).thenReturn(rate);
        cashService.save(rate);
        verify(rateRepository, times(1)).save(rate);
    }

    @Test
    void getRatesBalance() {
        when(rateRepository.getByBalanceLessThan(anyInt())).thenReturn(rates);
        final int size = rates.size();
        assertArrayEquals(cashService.getRatesBalance(1).toArray(new Rate[size]),
                rates.toArray(new Rate[size]));
        verify(rateRepository, times(1)).getByBalanceLessThan(anyInt());
    }
}