package com.white.config;

import com.whitekit.repository.RateRepository;
import com.whitekit.service.CashService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.mockito.Mockito.mock;

@Configuration
public class Config {
    @Bean
    public RateRepository rateRepository(){
        return mock(RateRepository.class);
    }
    @Bean
    public CashService cashService(){
        return new CashService(rateRepository());
    }
}
