package com.whitekit.controllers;

import com.whitekit.model.Tariff;
import com.whitekit.repository.TariffRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/")
public class MainController {
    private final TariffRepository tariffRepository;

    public MainController(TariffRepository tariffRepository) {
        this.tariffRepository = tariffRepository;
    }

    @GetMapping(
            produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE}
    )
    public List<Tariff> getTariffs() {
        return tariffRepository.findAll();
    }
}
